
import { SkTabsImpl }  from '../../sk-tabs/src/impl/sk-tabs-impl.js';

import { OPEN_AN, TITLE_AN } from "../../sk-tabs/src/sk-tabs.js";

import { DISABLED_AN } from "../../sk-core/src/sk-element.js";

export class JquerySkTabs extends SkTabsImpl {

    get prefix() {
        return 'jquery';
    }

    get suffix() {
        return 'tabs';
    }

    get tabsEl() {
        if (! this._tabsEl) {
            this._tabsEl = this.comp.el.querySelector('div#tabs');
        }
        return this._tabsEl;
    }

    set tabsEl(el) {
        this._tabsEl = el;
    }

    get titleBarEl() {
        if (! this._tabsTitleBarEl) {
            this._tabsTitleBarEl = this.tabsEl.querySelector('ul');
        }
        return this._tabsTitleBarEl;
    }

    set titleBarEl(el) {
        this._tabsTitleBarEl = el;
    }

    get tabsContentEl() {
        if (! this._titleContentEl) {
            this._titleContentEl = this.tabsEl.querySelector('.tabs-contents');
        }
        return this._titleContentEl;
    }

    set tabsContentEl(el) {
        this._titleContentEl = el;
    }

    get subEls() {
        return [ 'tabsEl', 'titleBarEl', 'tabsContentEl' ];
    }

    restoreState(state) {
        if (! this.comp.isTplSlotted) {
            this._tabsEl = null;
            this._tabsTitleBarEl = null;
            this._titleContentEl = null;
            this.tabsContentEl.innerHTML = '';
            this.titleBarEl.innerHTML = '';
            this.tabsContentEl.insertAdjacentHTML('beforeend', state.contentsState);
            this.renderTabs();
            this.bindTabSwitch();
            if (! this.titleBarEl.querySelector('[open]')) {
                this.updateTabs('tabs-1');
            }
        }
    }

    updateTabs(curTabId) {
        for (let tabId of Object.keys(this.tabs)) {
            let tab = this.tabs[tabId];
            let tabEl = this.titleBarEl.querySelector(`#header-${tab.dataset.tabId}`);
            if (tabId === curTabId) {
                tabEl.classList.add('ui-tabs-active', 'ui-state-active');
                this.tabs[tabId].style.display = 'block';
            } else {
                tabEl.classList.remove('ui-tabs-active', 'ui-state-active');
                this.tabs[tabId].style.display = 'none';
            }
        }
    }

    renderTabs(tabEls) {
        if (this.comp.isTplSlotted) {
            this.renderSlottedTabs(tabEls);
        } else {
            //this.tabsEl.insertAdjacentHTML('beforeend', this.contentsState || this.comp.contentsState);
            let tabs = tabEls || this.tabsEl.querySelectorAll(this.comp.tabSl);
            let num = 1;
            this.tabs = {};
            for (let tab of tabs) {
                let isOpen = tab.hasAttribute(OPEN_AN);
                let title = tab.getAttribute(TITLE_AN) ? tab.getAttribute(TITLE_AN) : '';
                this.titleBarEl.insertAdjacentHTML('beforeend', `
                    <li role="tab" tabindex="0" class="ui-tabs-tab ui-corner-top ${tab.hasAttribute(DISABLED_AN) ? 'ui-state-disabled' : 'ui-state-default'} ui-tab ${isOpen ? 'ui-tabs-active ui-state-active' : ''}" 
                        aria-controls="tabs-${num}" aria-labelledby="ui-id-${num}" aria-selected="true" aria-expanded="true" id="header-${num}">
                            <a href="#tabs-${num}" role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-${num}" ${isOpen ? 'open' : ''}>
                                ${title}
                            </a>
                    </li>`);
                this.tabsContentEl.insertAdjacentHTML('beforeend', `
                    <div id="tabs-${num}" aria-labelledby="ui-id-${num}" role="tabpanel" data-tab-id="${num}"
                        ${! isOpen ? 'style="display: none;"' : ''} 
                        class="ui-tabs-panel ui-corner-bottom ui-widget-content" aria-hidden="${! isOpen ? "true" : "false"}">
                        ${tab.outerHTML}
                    </div>
                `);
                this.removeEl(tab);
                this.tabs['tabs-' + num] = this.tabsEl.querySelector('#tabs-' + num);
                num++;
            }
        }
    }

    bindTabSwitch() {
        this.titleBarEl.querySelectorAll('a').forEach(function(link) {
            link.onclick = function(event) {
                if (this.comp.hasAttribute(DISABLED_AN) 
                    || event.target.classList.contains('ui-state-disabled')
                    || event.target.parentElement.classList.contains('ui-state-disabled')) {
                    return false;
                }
                let tabId = event.target.getAttribute('href').substr(1);
                this.updateTabs(tabId);
            }.bind(this);
        }.bind(this));
    }

    afterRendered() {
        super.afterRendered();
        this.mountStyles();
    }

    enable() {
        super.enable();
        this.tabsEl.classList.remove('ui-state-disabled');
    }

    disable() {
        super.disable();
        this.tabsEl.classList.add('ui-state-disabled');
    }
}
