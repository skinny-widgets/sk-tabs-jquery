
import { SkSltabsImpl }  from '../../sk-tabs/src/impl/sk-sltabs-impl.js';

export class JquerySkSltabs extends SkSltabsImpl {

    get prefix() {
        return 'jquery';
    }

    get suffix() {
        return 'sltabs';
    }

}
