
import { SkTabImpl }  from '../../sk-tabs/src/impl/sk-tab-impl.js';

export class JquerySkTab extends SkTabImpl {

    get prefix() {
        return 'jquery';
    }

    get suffix() {
        return 'tab';
    }

}
